// An example of a very simple test case to help check
// if add is adding in sorted order. It can be expanded
// and made more complex as further tests are needed.

public class SimpleTest {
    public static void main(String[] args) {
        SortedIntList list = new SortedIntList();
        
        list.add(1);
        list.add(5);
        list.add(-3);
        list.add(2);
        list.add(2);
        list.add(2);
        list.add(6);
        
        System.out.println("list = " + list);
        System.out.println(list.size());
    }
}
