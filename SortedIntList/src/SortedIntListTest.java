/**
 * @author Ryan Hendrickson
 * @date April 23rd, 2019.
 * @filename SortedIntListTest.java
 * @purpose java file that test our SortedIntList class to ensure it works properly.
 */

import java.util.*;

public class SortedIntListTest {
	public static void main(String[] args) {
		Random gen = new Random();

		SortedIntList list1 = new SortedIntList(true, 25);
		populate(list1, 25);
		list1.add(gen.nextInt(100));
		System.out.println(list1.toString());
		
		SortedIntList list2 = new SortedIntList(false, 25);
		populate(list2, 25);
		list2.remove(gen.nextInt(25));
		System.out.println(list2);
		
		list2.setUnique(true);
		System.out.println(list2.toString());
	}
	
	// method(s)
	
	// populate takes a SortedIntList object and fills it with random numbers
	public static void populate(SortedIntList a, int n) {
		Random gen = new Random();
		for(int i = 0; i < n; i++) {
			a.add(gen.nextInt((100/2)*2));
		}
	}
}














