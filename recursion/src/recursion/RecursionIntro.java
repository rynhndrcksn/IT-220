package recursion;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RecursionIntro {
	public static void main(String[] args) throws FileNotFoundException {
		//writeStarsLoop(5);
		//writeStars(5);
		
		File f = new File("data.txt");
		Scanner fileIn = new Scanner(f);
		reverseLines(fileIn);
	}
	
	public static void reverseLines(Scanner input) {
		if(input.hasNextLine()) {
			String line = input.nextLine();
			reverseLines(input);
			System.out.println(line);
		}
	}
	
	public static void countTo(int n) {
		if(n == 1) {
			System.out.println();
		} else {
			countTo(n-1);
		}
		System.out.println(n);
	}
	
	public static void writeStarsLoop(int n) {
		// prints one line with n stars on it
		
		for(int i = 0; i < n; i++) {
			System.out.print("*");
		}
		System.out.println();
	}
	
	public static void writeStars(int n) {
		// prints one line with n stars on it
		
		if(n == 0) {
			System.out.print(""); // base case
		} else {
			System.out.print("*");
			writeStars(n-1); // recursive case
		}
		
	}
}
