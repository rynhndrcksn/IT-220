package points;

public class PointExample1 {
	public static void main(String[] args) {
		Point p = new Point();
		p.setX(3);
		p.setY(8);
		System.out.println(p);
		
		p.translate(-1, -2);
		System.out.println(p);
	}
}
