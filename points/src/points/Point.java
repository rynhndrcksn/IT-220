package points;

public class Point {
	private int x;
	private int y;
	
	public Point() {
		x = 0;
		y = 0;
	}
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void translate(int x, int y) {
		this.x+=x;
		this.y+=y;
	}
	
	@Override
	public String toString() {
		return "Point x is at: " + x + "\nPoint y is at: " + y;
	}
}
