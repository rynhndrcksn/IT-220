package midterm;

public class ArrayIntListTester {
	public static void main(String[] args) {
		// make an ArrayIntList object and add values to it
		ArrayIntList a = new ArrayIntList();
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);
		
		// put our methods to the test
		System.out.println(a.getFirst());
		a.addFirst(0);
		System.out.println(a);
		System.out.println(a.isEmpty());
		a.mirror();
		System.out.println(a);
		a.doubleList();
		System.out.println(a);
	}
}
