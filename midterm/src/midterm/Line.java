package midterm;

public class Line {
	// fields
	private Point p1;
	private Point p2;
	
	// constructors
	public Line(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public Line(int x1, int y1, int x2, int y2) {
		this.p1 = new Point(x1, y1);
		this.p2 = new Point(x2, y2);
	}
	
	// methods
	
	public Point getP1() {
		return p1;
	}
	
	public Point getP2() {
		return p2;
	}
	
	public String toString() {
		return "[" + p1 + ", " + p2 + "]";
	}
	
	public boolean equals(Object o) {
		if(o instanceof Point) {
			Line other = (Line) o; // cast o to be a Point
			if(this.p1.equals(other.p1)) {
				if(this.p2.equals(other.p2)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public int deltaX() {
		int dif = p2.getX() - p1.getX();
		return dif;
	}
	
	public int deltaY() {
		int dif = p2.getY() - p1.getY();
		return dif;
	}
	
	public double slope() {
		if(deltaX() == 0) {
			throw new IllegalStateException("Slope is undefined if x is horizontal");
		} else {
			double y = (double) deltaY();
			double x = (double) deltaX();
			return (y/x);
		}
	}
}
