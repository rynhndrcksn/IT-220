package ch16;

public class ListNode {
	// fields | they're public for our tester class
	public int data;
	public ListNode next;
	
	// constructors | our 2 parameter constructor is on top
	public ListNode(int data, ListNode next) {
		this.data = data;
		this.next = next;
	}
	
	// we can pass our data to the above constructor and have next be null
	public ListNode(int data) {
		this(data, null);
	}
	
	// we can pass it even further and have it pass a 0 and null
	public ListNode() {
		this(0, null);
	}
}
