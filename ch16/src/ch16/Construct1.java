package ch16;

public class Construct1 {
	public static void main(String[] args) {
		ListNode list = new ListNode();
		list.data = 1;
		list.next = new ListNode();
		list.next.data = 7;
		list.next.next = new ListNode();
		list.next.next.data = 12;
		list.next.next.next = null;
		
		// or we could do this...
		ListNode list2 = new ListNode(3, new ListNode(7, new ListNode(12)));
		System.out.println(list2.data);
		System.out.println(list2.next.data);
		System.out.println(list2.next.next.data);
	}
}
