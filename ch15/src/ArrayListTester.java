import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ArrayListTester {
	
	public static void main(String[] args) {
		File f = new File("bands.txt");

		try {
			Scanner input = new Scanner(f);
		} catch (Exception e) {
			System.out.println("Error: " + e);
		}

		ArrayList<String> bands = new ArrayList<String>();

		// putting our scanner in a try/catch broke it. Thanks Ken
		while(input.hasNextLine()) {
			String s = input.nextLine();
			bands.add(s);
		}
		
		bands.add("Tool");
		bands.add("Eagle");
		bands.add("Foreigner");	
	}
}
