import java.awt.Color;


public class Bunny extends Critter {
	// fields
	private int timesMoved = 0;
	private boolean hungry = true;
	
	// constructors
	public Bunny() {
		
	}
	
	// methods
	public Color getColor() {
		return Color.DARK_GRAY;
	}
	
	public boolean eat() {
		if(hungry) {
			hungry = false;
			return true;
		} else {
			hungry = true;
			return false;
		}
	}
	
	public Attack fight(String opponent) {
		return Attack.SCRATCH;
	}
	
	public Direction getMove() {
		if(timesMoved == 6) {
			timesMoved = 0;
		}
		if(timesMoved >= 0 && timesMoved < 2) {
			timesMoved++;
			return Direction.NORTH;
		} else if(timesMoved >= 2 && timesMoved < 4) {
			timesMoved++;
			return Direction.SOUTH;
		} else if(timesMoved >= 4 && timesMoved < 6) {
			timesMoved++;
			return Direction.EAST;
		} else {
			return Direction.CENTER;
		}
	}
	
	public String toString() {
		return "V";
	}
}
