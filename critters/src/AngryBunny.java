import java.awt.Color;
import java.util.Random;

public class AngryBunny extends Bunny{
	// fields
	
	// constructors
	public AngryBunny() {
		
	}
	
	// methods
	public Color getColor() {
		return Color.RED;
	}
	
	public Attack fight(String opponent) {
		Random gen = new Random();
		int n = gen.nextInt(2);
		if(n == 0) {
			return Attack.POUNCE;
		} else {
			return Attack.ROAR;
		}
	}
}
