/**
 * @author Ryan Hendrickson
 * @date Wednesday, April 19th, 2019.
 * @filename Hippo.java
 * @purpose create our hippo class for the critters assignment.
 */

import java.awt.Color;
import java.util.Random;

public class Hippo extends Critter {
	// fields
	private int hunger = 0;
	private int timesMoved = 0;
	private int direction = 0;
	private Random gen = new Random();
	
	// constructor
	public Hippo(int hunger) {
		this.hunger = hunger;
	}
	
	// methods to override our critter ones
	public Color getColor() {
		if(hunger > 0) {
			return Color.GRAY;
		} else {
		return Color.WHITE;
		}
	}
	
	public boolean eat() {
		if(hunger > 0) {
			hunger--;
			return true;
		} else {
			return false;
		}
	}
	
	public Attack fight(String opponent) {
		if(hunger > 0) {
			return Attack.SCRATCH;
		} else {
			return Attack.POUNCE;
		}
	}
	
	public Direction getMove() {
		if(timesMoved == 5) {
			timesMoved = 0;
		}
		if(timesMoved == 0) {
			direction = gen.nextInt(4);
		}
		if(direction == 0) {
			timesMoved++;
			return Direction.NORTH;
		} else if(direction == 1) {
			timesMoved++;
			return Direction.EAST;
		} else if(direction == 2) {
			timesMoved++;
			return Direction.SOUTH;
		} else if(direction == 3) {
			timesMoved++;
			return Direction.WEST;
		} else {
			return Direction.CENTER;
		}
	}
	
	public String toString() {
		return "" + hunger;
	}
}
