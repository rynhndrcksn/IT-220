/**
 * @author Ryan Hendrickson
 * @date Wednesday, April 19th, 2019.
 * @filename Bird.java
 * @purpose create our bird class for the critters assignment.
 */
import java.awt.*;

public class Bird extends Critter {
	// fields
	private int timesMoved = 0;
	
	// constructor
	public Bird() {
		
	}
	
	// override the Critter methods with our specific ones
	public Color getColor() {
		return Color.BLUE;
	}
	
	public boolean eat() {
		return false;
	}
	
	public Attack fight(String opponent) {
		// if it's an ant, we roar
		if(opponent.equalsIgnoreCase("%")) {
			return Attack.ROAR;
		} else {
			return Attack.POUNCE;
		}
	}
	
	public Direction getMove() {
		if(timesMoved >= 0 && timesMoved < 3) {
			timesMoved++;
			return Direction.NORTH;
		} else if(timesMoved >= 3 && timesMoved < 6) {
			timesMoved++;
			return Direction.EAST;
		} else if(timesMoved >= 6 && timesMoved < 9) {
			timesMoved++;
			return Direction.SOUTH;
		} else if(timesMoved >= 9 && timesMoved < 12) {
			timesMoved++;
			return Direction.WEST;
		} else {
			timesMoved = 0;
			return Direction.CENTER;
		}
	}
	
	public String toString() {
		if(timesMoved >= 0 && timesMoved < 3) {
			return "^";
		} else if(timesMoved >= 3 && timesMoved < 6) {
			return ">";
		} else if(timesMoved >= 6 && timesMoved < 9) {
			return "V";
		} else if(timesMoved >= 9 && timesMoved < 12) {
			return "<";
		} else {
			return "^";
		}
	}
}
