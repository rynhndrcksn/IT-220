/**
 * @author Ryan Hendrickson
 * @date Wednesday, April 19th, 2019.
 * @filename Husky.java
 * @purpose create our husky class for the critters assignment.
 */

import java.awt.Color;
import java.util.Random;

public class Husky extends Critter {
	// fields
	private int num = 0;
	private Random gen = new Random();
	
	// constructor
	public Husky() {
		
	}
	
	// override our critter methods with our own
	public Color getColor() {
		return Color.GRAY;
	}
	
	public boolean eat() {
		return true;
	}
	
	// our Husky will do random attacks to keep things interesting
	public Attack fight(String opponent) {
		num = gen.nextInt(2);
		if(num == 0) {
			return Attack.POUNCE;
		} else if( num == 1) {
			return Attack.ROAR;
		} else {
			return Attack.SCRATCH;
		}
	}
	
	// our Husky will randomly run around as Huskies tend to do
	public Direction getMove() {
		num = gen.nextInt(4);
		if(num == 0) {
			return Direction.NORTH;
		} else if(num == 1) {
			return Direction.EAST;
		} else if(num == 2) {
			return Direction.SOUTH;
		} else if(num == 3) {
			return Direction.WEST;
		} else {
			return Direction.CENTER;
		}
	}
	
	public String toString() {
		return "#";
	}
}
