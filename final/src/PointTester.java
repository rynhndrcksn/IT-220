
public class PointTester {
	public static void main(String[] args) {
		// create our points
		ThreeDimensionalPoint p1 = new ThreeDimensionalPoint(1,1,1);
		ThreeDimensionalPoint p2 = new ThreeDimensionalPoint(1,1,1);
		ThreeDimensionalPoint p3 = new ThreeDimensionalPoint(1,1,1);
		
		// make sure our points have the right coordinates, then test translate and verify it works
		System.out.println(p1);
		System.out.println(p2);
		p2.translate(4, 4, 4);
		System.out.println(p2);
		
		// verify our equals method works
		System.out.println(p1.equals(p2));
		System.out.println(p1.equals(p3));
		
		// verify our compareTo method works
		System.out.println(p1.compareTo(p2));
		System.out.println(p1.compareTo(p3));
		
		// and everything works as tested!
	}
}
