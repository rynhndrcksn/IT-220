/*
 * Author: Ryan Hendrickson
 * Date: June 10th, 2019.
 * Filename: Problem3Tester.java
 * Purpose: Test the new method we add to our LinkedIntList.java
 * Note: I used Ken's tester instead of my own.
 */

// Name:
// IT 220 Programming 2
// Problem 3 - Linked List Programming

// Tester for LinkedIntList
public class Problem3Tester {

   public static void main(String[] args) {
   
      Problem3Tester p3 = new Problem3Tester();
   
      // Test the example from the instruction sheet
      if (p3.testExampleFromInstructions()) {
         System.out.println("PASS: example from instructions");
      }
      else {
         System.out.println("FAIL: example from instructions");
      }
      
//      // Test the case when the list is empty
//      if (p3.testEmptyList()) {
//         System.out.println("PASS: case of empty list");
//      }
//      else {
//         System.out.println("FAIL: case of empty list");
//      }
      
//      // Test the case when the list has only one node
//      if (p3.testListWithOneNode()) {
//         System.out.println("PASS: case of one node");
//      }
//      else {
//         System.out.println("FAIL: case of one node");
//      }
      
      // Test the case when the list has two nodes
      // ...
      
   } // end method main 
   
   
   // This method tests the example from the instructions.
   // Returns true if it works, returns false if it doesn't.
   public boolean testExampleFromInstructions() {
      // set up the list
      LinkedIntList list = new LinkedIntList();
      list.add(18);
      list.add(4);
      list.add(27);
      list.add(9);
      list.add(54);
      list.add(5);
      list.add(63);
      
      int originalSize = 7;
      
      // call the method
      list.backToFront();
      
      // check to see if we got the correct results
      
      // if the list size changed, something went wrong
      if (list.size() != originalSize) {
         // return false to indicate fail 
    	  System.out.println("Size is wrong");
         return false;
      }
      
      // check to see if the values are correct      
      if (list.get(0) == 63 &&
          list.get(1) == 18 &&
          list.get(2) == 4 &&
          list.get(3) == 27 &&
          list.get(4) == 9 &&
          list.get(5) == 54 &&
          list.get(6) == 5) {
          
    	  System.out.println("All values match and were moved correctly");
         return true;
      }
      else {
    	  System.out.println("Values don't match");
         return false;
      }
   }

   
   public boolean testEmptyList() {
      // your test code here
      return false;  // replace this line
   }

   
   public boolean testListWithOneNode() {
      // your test code here
      return false;  // replace this line
   }
  

} // end class Problem3Tester