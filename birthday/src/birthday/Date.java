/*
 * Author: Ryan Hendrickson
 * Date: April 4th, 2019.
 * Purpose: create a Date class for our birthday class that allows us to 
 * do all the fancy stuff we want to do like make dates and calculate
 * how long until birthdays and what not.
 */

package birthday;

public class Date {
	// attributes
	private int month;
	private int day;
	// make an array of int's that matches months to their days
	private int[] daysInMonths = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	// default constructor followed by constructor with parameters
	public Date() {
		month = 0;
		day = 0;
	}
	public Date(int month, int day) {
		this.month = month;
		this.day = day;
	}
	
	// accessors
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	// mutators
	public void setDate(int month, int day) {
		this.month = month;
		this.day = day;
	}
	

	// override toString
	@Override
	public String toString() {
		return this.getMonth() + "/" + this.getDay();
	}
	
	// check to see if our Date objects are the same
	public boolean equals(Date d) {
		if(this.month == d.month && this.day == d.day) {
			return true;
		} else {
			return false;
		}
	}
	
	// grab the amount of days in the month by using our array
	// then calling our .getMonth for the index of our array
	public int daysInMonth() {
		return daysInMonths[this.getMonth()];
	}
	
	// advance the day of the month by 1, then get the number of days in that month
	// if our day variable is greater than the amount of days in the month then
	// we set day to 1 and advance the month | if month hits 13, we set it to 1
	public void nextDay() {
		this.day += 1;
		int days = daysInMonths[this.getMonth()];
		if(this.day > days) {
			this.month +=1;
			this.day = 1;
		}
		if(this.getMonth() > 12) {
			this.month = 1;
		}
	}
}
