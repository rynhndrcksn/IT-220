package trees;

public class IntTree {
	// fields
	private IntTreeNode overallRoot;
	
	// constructors
	public IntTree() {
		// create a tree that has some nodes in it
		overallRoot = new IntTreeNode(17);
		overallRoot.left = new IntTreeNode(41);
		overallRoot.right = new IntTreeNode(9);
		overallRoot.left.left = new IntTreeNode(29);
		overallRoot.left.right = new IntTreeNode(6);
		overallRoot.right.left = new IntTreeNode(81);
		overallRoot.right.right = new IntTreeNode(40);
	}
	
	// methods
	public void print() {
		print(overallRoot);
		System.out.println();
	}
	
	// private helper method
	private void print(IntTreeNode root) {
		if(root != null) {
			// using this we will print the left subtree, the root's data, then the right subtree
			print(root.left);
			System.out.println(root.data + " ");
			print(root.right);
		} else {
			// do nothing
		}
	}
	
	// ADD printSideways HERE
}
